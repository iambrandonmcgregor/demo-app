 <div class="center">
<p align="center"><img src="https://user-images.githubusercontent.com/523933/49741959-91a1da00-fc65-11e8-911f-521331f87174.png" align="center" width="20%" height="20%"></p>
  <h3 align="center">Clear Street</h3>
  <p align="center">
  Front-end Developer Screening
</p>
</div>

---

This repository contains parts of your interview. This is your own private version of this repository to complete your work. Only you and and Clear Street members can view your repository.

Take a branch of `master` to do your work. Create a pull-request with your notes and merge your work into `master` to signal the team that you are ready for us to evaluate your project. Make sure we have complete instructions for how to compile and run your project. 

## goals

The goal of this exercise is to practice designing models and interfaces, and to get a feel for how you architect front-end code.

There aren't good or bad solutions; rather, there are solutions that match the requirements and some that don't. There are solutions that might be considered elegant by some and solutions that would be considered clever. We want to see you iterate on this and show us what the best of what you do!

Given the following mock up please create a sample application that meets the preceding requirements.

![screenshot 2018-12-10 at 4 04 02 pm](https://user-images.githubusercontent.com/523933/49761484-40a9da00-fc95-11e8-9559-e9ef9c288634.png)


## expectations

- Please set aside 4 hours for this exercise. If you can't complete the project in time that's fine just let us know:
  - Remaining items to be finished
- If we need to run the code on our end please provide instructions on how to do so.
- Don't stress out over this exercise, it's a chance to open a conversation about your work on a real project.

#### required features
- [ ] Should be built with Javascript, as single page application or isomorphic app
- [ ] The ability to see a list of symbols with Name, Price, Market Time, Intraday High/Low
- [ ] Tickers shouldn't be hardcoded, you can pull from a JSON file, Use a 3rd party API or Craft your own API
- [ ] Should be able to sort the page by Gainers/Losers
- [ ] Should be able to click on a symbol and view it on it's own page

#### What Can I use/What are you looking for?
- Should include a README explaining your process
- Use the tools you are comfortable with. You can use any framework, plugins or libraries you like. You can write this entirely in vanilla JS if you really want to.
- Please use this exercise as a way to show us what you like about Frontend Development. Some people live for performance, others for a11y, or great UX or writing tests. We are aware of the time constraint, if what you love takes time, put comments in.

## Clear Street Evaluation
- How much time did you spend on the exercise, what parts took longer?
- What were the hard parts, what parts did you enjoy most?
- What parts of the code are you proud of, were there any novel solutions you created?
- Is your code tested? Why/why not? How would you test it (or test it better)?
